<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Thomas">
<packages>
<package name="RESISTOR_THRUHOLE">
<pad name="A" x="-3.81" y="0" drill="0.75"/>
<pad name="B" x="3.81" y="0" drill="0.75"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
<text x="0" y="0" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<rectangle x1="-2.54" y1="-1.27" x2="2.54" y2="1.27" layer="39"/>
</package>
<package name="LED_5MM">
<pad name="A" x="-1.27" y="0" drill="1"/>
<pad name="C" x="1.27" y="0" drill="1" shape="square"/>
<wire x1="2.413" y1="-1.143" x2="2.413" y2="1.143" width="0.127" layer="21" curve="-309.093859"/>
<wire x1="2.413" y1="1.143" x2="2.413" y2="-1.143" width="0.127" layer="21"/>
<circle x="0" y="0" radius="2.54" width="0" layer="39"/>
<text x="0" y="1.27" size="0.8128" layer="25" align="center">&gt;NAME</text>
<wire x1="-1.27" y1="-1.651" x2="-1.27" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-1.651" y1="-1.27" x2="-0.889" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0.889" y1="-1.27" x2="1.651" y2="-1.27" width="0.127" layer="21"/>
</package>
<package name="USB_MINI-B_5PF_90DEG_THRUHOLE">
<description>Mini USB Type B 5PF
Female Connector Receptacle SMT/DIP 90°Angled/Tail Board PCB Socket
Ebay/China</description>
<pad name="3" x="0" y="0" drill="0.7" diameter="1.016"/>
<pad name="4" x="0.8" y="-1.2" drill="0.7" diameter="1.016"/>
<pad name="5" x="1.6" y="0" drill="0.7" diameter="1.016"/>
<pad name="2" x="-0.8" y="-1.2" drill="0.7" diameter="1.016"/>
<pad name="1" x="-1.6" y="0" drill="0.7" diameter="1.016"/>
<pad name="SHIELD@1" x="-3.65" y="-5.6" drill="1.8"/>
<pad name="SHIELD@2" x="3.65" y="-5.6" drill="1.8"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-9.525" width="0.127" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="-9.525" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-9.525" x2="3.81" y2="-9.525" width="0.127" layer="21"/>
<wire x1="3.81" y1="0" x2="2.54" y2="0" width="0.127" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0" x2="-3.81" y2="0" width="0.127" layer="21"/>
<rectangle x1="-3.81" y1="-9.525" x2="3.81" y2="0" layer="39"/>
<text x="5.08" y="-8.89" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="TOGGLESWITCH_SPDT_90DEG_2MMPITCH">
<description>C&amp;K OS102011MA1QN1
(Mouser 611-OS102011MA1QN1)</description>
<pad name="A" x="-2" y="0" drill="0.9" shape="offset" rot="R270"/>
<pad name="N" x="0" y="0" drill="0.9" shape="offset" rot="R270"/>
<pad name="B" x="2" y="0" drill="0.9" shape="offset" rot="R270"/>
<wire x1="-4.3" y1="2.54" x2="4.3" y2="2.54" width="0.127" layer="21"/>
<wire x1="4.3" y1="2.54" x2="4.3" y2="-2.54" width="0.127" layer="21"/>
<wire x1="4.3" y1="-2.54" x2="-4.3" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-4.3" y1="-2.54" x2="-4.3" y2="2.54" width="0.127" layer="21"/>
<rectangle x1="-4.3" y1="-2.54" x2="4.3" y2="2.54" layer="39"/>
<text x="-5.08" y="-5.08" size="1.27" layer="25">&gt;NAME</text>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="5.08" width="0.127" layer="25"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.127" layer="25"/>
<wire x1="0" y1="5.08" x2="0" y2="2.54" width="0.127" layer="25"/>
<pad name="GND@1" x="-4.1" y="0" drill="1.3" shape="square"/>
<pad name="GND@2" x="4.1" y="0" drill="1.3" shape="square"/>
<rectangle x1="-2.54" y1="2.54" x2="2.54" y2="6.35" layer="39"/>
</package>
<package name="TOGGLESWITCH_SPDT_90DEG_3MMPITCH">
<pad name="A" x="-3" y="0" drill="1" shape="offset" rot="R270"/>
<pad name="N" x="0" y="0" drill="1" shape="offset" rot="R270"/>
<pad name="B" x="3" y="0" drill="1" shape="offset" rot="R270"/>
<wire x1="-6.35" y1="6.35" x2="6.35" y2="6.35" width="0.127" layer="21"/>
<wire x1="6.35" y1="6.35" x2="6.35" y2="0" width="0.127" layer="21"/>
<wire x1="6.35" y1="0" x2="-6.35" y2="0" width="0.127" layer="21"/>
<wire x1="-6.35" y1="0" x2="-6.35" y2="6.35" width="0.127" layer="21"/>
<text x="-5.08" y="-5.08" size="1.27" layer="25">&gt;NAME</text>
<wire x1="-2.54" y1="6.35" x2="-2.54" y2="10.16" width="0.127" layer="25"/>
<wire x1="-2.54" y1="10.16" x2="-1.27" y2="10.16" width="0.127" layer="25"/>
<wire x1="-1.27" y1="10.16" x2="-1.27" y2="6.35" width="0.127" layer="25"/>
<pad name="GND@1" x="-6" y="0" drill="1.3" shape="square"/>
<pad name="GND@2" x="6" y="0" drill="1.3" shape="square"/>
<rectangle x1="-3.175" y1="6.35" x2="3.175" y2="10.16" layer="39"/>
<rectangle x1="-6.35" y1="0" x2="6.35" y2="6.35" layer="39"/>
</package>
<package name="USB_MINI-B_5PF_90DEG_THRUHOLE_TE">
<description>Mini USB Type B 5PF
Female Connector Receptacle SMT/DIP 90°Angled/Tail Board PCB Socket
TE Connectivity 2172034-1, Mouser 571-2172034-1
Also works with generic receptacle from ebay</description>
<pad name="3" x="0" y="0" drill="0.7" diameter="1.016"/>
<pad name="4" x="0.8" y="-1.2" drill="0.7" diameter="1.016"/>
<pad name="5" x="1.6" y="0" drill="0.7" diameter="1.016"/>
<pad name="2" x="-0.8" y="-1.2" drill="0.7" diameter="1.016"/>
<pad name="1" x="-1.6" y="0" drill="0.7" diameter="1.016"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-9.525" width="0.127" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="-9.525" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-9.525" x2="3.81" y2="-9.525" width="0.127" layer="21"/>
<wire x1="3.81" y1="0" x2="2.54" y2="0" width="0.127" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0" x2="-3.81" y2="0" width="0.127" layer="21"/>
<rectangle x1="-3.81" y1="-9.525" x2="3.81" y2="0" layer="39"/>
<text x="0" y="-2.54" size="1.27" layer="25" align="top-center">&gt;NAME</text>
<wire x1="-4" y1="-4.1" x2="-4" y2="-6" width="0" layer="20"/>
<wire x1="-4" y1="-6" x2="-3.3" y2="-6" width="0" layer="20"/>
<wire x1="-3.3" y1="-6" x2="-3.3" y2="-4.1" width="0" layer="20"/>
<wire x1="-3.3" y1="-4.1" x2="-4" y2="-4.1" width="0" layer="20"/>
<wire x1="3.3" y1="-4.1" x2="3.3" y2="-6" width="0" layer="20"/>
<wire x1="4" y1="-6" x2="4" y2="-4.1" width="0" layer="20"/>
<wire x1="4" y1="-4.1" x2="3.3" y2="-4.1" width="0" layer="20"/>
<wire x1="3.3" y1="-6" x2="4" y2="-6" width="0" layer="20"/>
<wire x1="-3.81" y1="-6.8" x2="3.81" y2="-6.8" width="0.127" layer="51" style="shortdash"/>
<text x="0" y="-6.985" size="0.4064" layer="51" align="top-center">PCB edge</text>
<rectangle x1="2.54" y1="-6.35" x2="4.445" y2="-3.81" layer="16"/>
<rectangle x1="2.54" y1="-6.35" x2="4.445" y2="-3.81" layer="30"/>
<rectangle x1="-4.445" y1="-6.35" x2="-2.54" y2="-3.81" layer="30"/>
<rectangle x1="-4.445" y1="-6.35" x2="-2.54" y2="-3.81" layer="16"/>
<wire x1="4" y1="0.345" x2="3.3" y2="0.345" width="0" layer="20"/>
<wire x1="3.3" y1="0.345" x2="3.3" y2="-1.555" width="0" layer="20"/>
<wire x1="3.3" y1="-1.555" x2="4" y2="-1.555" width="0" layer="20"/>
<wire x1="4" y1="-1.555" x2="4" y2="0.345" width="0" layer="20"/>
<wire x1="-3.3" y1="0.345" x2="-4" y2="0.345" width="0" layer="20"/>
<wire x1="-4" y1="-1.555" x2="-3.3" y2="-1.555" width="0" layer="20"/>
<wire x1="-4" y1="0.345" x2="-4" y2="-1.555" width="0" layer="20"/>
<wire x1="-3.3" y1="-1.555" x2="-3.3" y2="0.345" width="0" layer="20"/>
<rectangle x1="2.54" y1="-1.905" x2="4.445" y2="0.635" layer="30"/>
<rectangle x1="2.54" y1="-1.905" x2="4.445" y2="0.635" layer="16"/>
<rectangle x1="-4.445" y1="-1.905" x2="-2.54" y2="0.635" layer="30"/>
<rectangle x1="-4.445" y1="-1.905" x2="-2.54" y2="0.635" layer="16"/>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="-0.762" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<text x="0" y="1.27" size="1.778" layer="95" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.27" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
<symbol name="LED">
<pin name="A" x="0" y="-7.62" visible="off" length="middle" rot="R90"/>
<pin name="C" x="0" y="7.62" visible="off" length="middle" rot="R270"/>
<wire x1="-2.54" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<text x="-2.54" y="5.08" size="1.778" layer="95" align="bottom-right">&gt;NAME</text>
<wire x1="-2.54" y1="0" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-4.318" y1="2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="1.778" width="0.254" layer="94"/>
<wire x1="-3.302" y1="-1.27" x2="-5.842" y2="1.27" width="0.254" layer="94"/>
<wire x1="-5.08" y1="1.27" x2="-5.842" y2="1.27" width="0.254" layer="94"/>
<wire x1="-5.842" y1="1.27" x2="-5.842" y2="0.508" width="0.254" layer="94"/>
</symbol>
<symbol name="TOGGLESWITCH_SPDT">
<circle x="2.54" y="5.08" radius="0.762" width="0.254" layer="94"/>
<pin name="N" x="-7.62" y="0" visible="off" length="middle"/>
<pin name="A" x="7.62" y="5.08" visible="off" length="middle" rot="R180"/>
<circle x="-2.54" y="0" radius="0.762" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="3.556" y2="4.572" width="0.254" layer="94"/>
<circle x="2.54" y="-5.08" radius="0.762" width="0.254" layer="94"/>
<pin name="B" x="7.62" y="-5.08" visible="off" length="middle" rot="R180"/>
<text x="-2.54" y="2.54" size="1.778" layer="95" align="bottom-right">&gt;NAME</text>
</symbol>
<symbol name="GND">
<pin name="GND" x="0" y="0" visible="off" length="short" rot="R270"/>
<wire x1="-1.27" y1="-3.81" x2="1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-0.254" y1="-5.08" x2="0.254" y2="-5.08" width="0.254" layer="94"/>
</symbol>
<symbol name="USB_CONN_NOSHIELD">
<pin name="1_VCC" x="-7.62" y="5.08" visible="pin"/>
<pin name="2_D-" x="-7.62" y="2.54" visible="pin"/>
<pin name="3_D+" x="-7.62" y="0" visible="pin"/>
<pin name="4_ID" x="-7.62" y="-2.54" visible="pin"/>
<pin name="5_GND" x="-7.62" y="-5.08" visible="pin"/>
<wire x1="-2.54" y1="6.35" x2="1.27" y2="7.62" width="0.254" layer="94"/>
<wire x1="1.27" y1="7.62" x2="1.27" y2="-7.62" width="0.254" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="-2.54" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-6.35" x2="-2.54" y2="6.35" width="0.254" layer="94"/>
<wire x1="3.81" y1="10.16" x2="3.81" y2="-10.16" width="0.254" layer="94"/>
<wire x1="3.81" y1="-10.16" x2="0" y2="-10.16" width="0.254" layer="94"/>
<wire x1="0" y1="-10.16" x2="-2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="0" y2="10.16" width="0.254" layer="94"/>
<wire x1="0" y1="10.16" x2="3.81" y2="10.16" width="0.254" layer="94"/>
<text x="-5.08" y="10.668" size="1.778" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="THRUHOLE" package="RESISTOR_THRUHOLE">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="B"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED_5MM" prefix="D">
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="5MM_THRUHOLE" package="LED_5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TOGGLESWITCH_SPDT_90DEG_2MMPITCH" prefix="S">
<gates>
<gate name="G$2" symbol="GND" x="0" y="17.78"/>
<gate name="G$1" symbol="TOGGLESWITCH_SPDT" x="0" y="5.08"/>
</gates>
<devices>
<device name="" package="TOGGLESWITCH_SPDT_90DEG_2MMPITCH">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="N" pad="N"/>
<connect gate="G$2" pin="GND" pad="GND@1 GND@2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="TOGGLESWITCH_SPDT_90DEG_3MMPITCH">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="N" pad="N"/>
<connect gate="G$2" pin="GND" pad="GND@1 GND@2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="USB_NOSHIELD" prefix="CN">
<gates>
<gate name="G$1" symbol="USB_CONN_NOSHIELD" x="2.54" y="0"/>
</gates>
<devices>
<device name="KINA_2MNTHOLES" package="USB_MINI-B_5PF_90DEG_THRUHOLE">
<connects>
<connect gate="G$1" pin="1_VCC" pad="1"/>
<connect gate="G$1" pin="2_D-" pad="2"/>
<connect gate="G$1" pin="3_D+" pad="3"/>
<connect gate="G$1" pin="4_ID" pad="4"/>
<connect gate="G$1" pin="5_GND" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TE2172034-1" package="USB_MINI-B_5PF_90DEG_THRUHOLE_TE">
<connects>
<connect gate="G$1" pin="1_VCC" pad="1"/>
<connect gate="G$1" pin="2_D-" pad="2"/>
<connect gate="G$1" pin="3_D+" pad="3"/>
<connect gate="G$1" pin="4_ID" pad="4"/>
<connect gate="G$1" pin="5_GND" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="R1" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="D1" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D2" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D3" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D4" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D5" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D6" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D7" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D8" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D9" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D10" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D11" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D12" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D13" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D14" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D15" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D16" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D17" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D18" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D19" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D20" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D21" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D22" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D23" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D24" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D25" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D26" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D27" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D28" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D29" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D30" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D31" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D32" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D33" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D34" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D35" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D36" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D37" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D38" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D39" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D40" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D41" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D42" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D43" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D44" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D45" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D46" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D47" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D48" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D49" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D50" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D51" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D52" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D53" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D54" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D55" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D56" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D57" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D58" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D59" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D60" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D61" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D62" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D63" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D64" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D65" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D66" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D67" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D68" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D69" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D70" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D71" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D72" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D73" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D74" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D75" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D76" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D77" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D78" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D79" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D80" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D81" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D82" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D83" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D84" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D85" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D86" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D87" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D88" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D89" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D90" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D91" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D92" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D93" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D94" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D95" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D96" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D97" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D98" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D99" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="D100" library="Thomas" deviceset="LED_5MM" device="5MM_THRUHOLE"/>
<part name="R2" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R3" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R4" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R5" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R6" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R7" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R8" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R9" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R10" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R11" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R12" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R13" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R14" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R15" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R16" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R17" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R18" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R19" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R20" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R21" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R22" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R23" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R24" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R25" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R26" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R27" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R28" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R29" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R30" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R31" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R32" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R33" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R34" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R35" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R36" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R37" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R38" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R39" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R40" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R41" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R42" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R43" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R44" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R45" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R46" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R47" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R48" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R49" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R50" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R51" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R52" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R53" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R54" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R55" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R56" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R57" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R58" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R59" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R60" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R61" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R62" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R63" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R64" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R65" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R66" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R67" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R68" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R69" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R70" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R71" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R72" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R73" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R74" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R75" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R76" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R77" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R78" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R79" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R80" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R81" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R82" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R83" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R84" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R85" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R86" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R87" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R88" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R89" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R90" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R91" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R92" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R93" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R94" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R95" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R96" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R97" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R98" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R99" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="R100" library="Thomas" deviceset="RESISTOR" device="THRUHOLE" value="2K"/>
<part name="ON" library="Thomas" deviceset="TOGGLESWITCH_SPDT_90DEG_2MMPITCH" device=""/>
<part name="USB_PWR" library="Thomas" deviceset="USB_NOSHIELD" device="TE2172034-1"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="R1" gate="G$1" x="-2.54" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="-5.08" y="25.4" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="0" y="25.4" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="D1" gate="G$1" x="-2.54" y="48.26" smashed="yes">
<attribute name="NAME" x="-5.08" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D2" gate="G$1" x="7.62" y="48.26" smashed="yes">
<attribute name="NAME" x="5.08" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D3" gate="G$1" x="17.78" y="48.26" smashed="yes">
<attribute name="NAME" x="15.24" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D4" gate="G$1" x="27.94" y="48.26" smashed="yes">
<attribute name="NAME" x="25.4" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D5" gate="G$1" x="38.1" y="48.26" smashed="yes">
<attribute name="NAME" x="35.56" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D6" gate="G$1" x="48.26" y="48.26" smashed="yes">
<attribute name="NAME" x="45.72" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D7" gate="G$1" x="58.42" y="48.26" smashed="yes">
<attribute name="NAME" x="55.88" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D8" gate="G$1" x="68.58" y="48.26" smashed="yes">
<attribute name="NAME" x="66.04" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D9" gate="G$1" x="78.74" y="48.26" smashed="yes">
<attribute name="NAME" x="76.2" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D10" gate="G$1" x="88.9" y="48.26" smashed="yes">
<attribute name="NAME" x="86.36" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D11" gate="G$1" x="99.06" y="48.26" smashed="yes">
<attribute name="NAME" x="96.52" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D12" gate="G$1" x="109.22" y="48.26" smashed="yes">
<attribute name="NAME" x="106.68" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D13" gate="G$1" x="119.38" y="48.26" smashed="yes">
<attribute name="NAME" x="116.84" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D14" gate="G$1" x="129.54" y="48.26" smashed="yes">
<attribute name="NAME" x="127" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D15" gate="G$1" x="139.7" y="48.26" smashed="yes">
<attribute name="NAME" x="137.16" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D16" gate="G$1" x="149.86" y="48.26" smashed="yes">
<attribute name="NAME" x="147.32" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D17" gate="G$1" x="160.02" y="48.26" smashed="yes">
<attribute name="NAME" x="157.48" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D18" gate="G$1" x="170.18" y="48.26" smashed="yes">
<attribute name="NAME" x="167.64" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D19" gate="G$1" x="180.34" y="48.26" smashed="yes">
<attribute name="NAME" x="177.8" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D20" gate="G$1" x="190.5" y="48.26" smashed="yes">
<attribute name="NAME" x="187.96" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D21" gate="G$1" x="200.66" y="48.26" smashed="yes">
<attribute name="NAME" x="198.12" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D22" gate="G$1" x="210.82" y="48.26" smashed="yes">
<attribute name="NAME" x="208.28" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D23" gate="G$1" x="220.98" y="48.26" smashed="yes">
<attribute name="NAME" x="218.44" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D24" gate="G$1" x="231.14" y="48.26" smashed="yes">
<attribute name="NAME" x="228.6" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D25" gate="G$1" x="241.3" y="48.26" smashed="yes">
<attribute name="NAME" x="238.76" y="53.34" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D26" gate="G$1" x="-2.54" y="96.52" smashed="yes">
<attribute name="NAME" x="-5.08" y="101.6" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D27" gate="G$1" x="7.62" y="96.52" smashed="yes">
<attribute name="NAME" x="5.08" y="101.6" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D28" gate="G$1" x="17.78" y="96.52" smashed="yes">
<attribute name="NAME" x="15.24" y="101.6" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D29" gate="G$1" x="27.94" y="96.52" smashed="yes">
<attribute name="NAME" x="25.4" y="101.6" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D30" gate="G$1" x="38.1" y="96.52" smashed="yes">
<attribute name="NAME" x="35.56" y="101.6" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D31" gate="G$1" x="48.26" y="96.52" smashed="yes">
<attribute name="NAME" x="45.72" y="101.6" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D32" gate="G$1" x="58.42" y="96.52" smashed="yes">
<attribute name="NAME" x="55.88" y="101.6" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D33" gate="G$1" x="68.58" y="96.52" smashed="yes">
<attribute name="NAME" x="66.04" y="101.6" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D34" gate="G$1" x="78.74" y="96.52" smashed="yes">
<attribute name="NAME" x="76.2" y="101.6" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D35" gate="G$1" x="88.9" y="96.52" smashed="yes">
<attribute name="NAME" x="86.36" y="101.6" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D36" gate="G$1" x="99.06" y="96.52" smashed="yes">
<attribute name="NAME" x="96.52" y="101.6" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D37" gate="G$1" x="109.22" y="96.52" smashed="yes">
<attribute name="NAME" x="106.68" y="101.6" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D38" gate="G$1" x="119.38" y="96.52" smashed="yes">
<attribute name="NAME" x="116.84" y="101.6" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D39" gate="G$1" x="129.54" y="96.52" smashed="yes">
<attribute name="NAME" x="127" y="101.6" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D40" gate="G$1" x="139.7" y="96.52" smashed="yes">
<attribute name="NAME" x="137.16" y="101.6" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D41" gate="G$1" x="149.86" y="96.52" smashed="yes">
<attribute name="NAME" x="147.32" y="101.6" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D42" gate="G$1" x="160.02" y="96.52" smashed="yes">
<attribute name="NAME" x="157.48" y="101.6" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D43" gate="G$1" x="170.18" y="96.52" smashed="yes">
<attribute name="NAME" x="167.64" y="101.6" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D44" gate="G$1" x="180.34" y="96.52" smashed="yes">
<attribute name="NAME" x="177.8" y="101.6" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D45" gate="G$1" x="190.5" y="96.52" smashed="yes">
<attribute name="NAME" x="187.96" y="101.6" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D46" gate="G$1" x="200.66" y="96.52" smashed="yes">
<attribute name="NAME" x="198.12" y="101.6" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D47" gate="G$1" x="210.82" y="96.52" smashed="yes">
<attribute name="NAME" x="208.28" y="101.6" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D48" gate="G$1" x="220.98" y="96.52" smashed="yes">
<attribute name="NAME" x="218.44" y="101.6" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D49" gate="G$1" x="231.14" y="96.52" smashed="yes">
<attribute name="NAME" x="228.6" y="101.6" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D50" gate="G$1" x="241.3" y="96.52" smashed="yes">
<attribute name="NAME" x="238.76" y="101.6" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D51" gate="G$1" x="-2.54" y="144.78" smashed="yes">
<attribute name="NAME" x="-5.08" y="149.86" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D52" gate="G$1" x="7.62" y="144.78" smashed="yes">
<attribute name="NAME" x="5.08" y="149.86" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D53" gate="G$1" x="17.78" y="144.78" smashed="yes">
<attribute name="NAME" x="15.24" y="149.86" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D54" gate="G$1" x="27.94" y="144.78" smashed="yes">
<attribute name="NAME" x="25.4" y="149.86" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D55" gate="G$1" x="38.1" y="144.78" smashed="yes">
<attribute name="NAME" x="35.56" y="149.86" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D56" gate="G$1" x="48.26" y="144.78" smashed="yes">
<attribute name="NAME" x="45.72" y="149.86" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D57" gate="G$1" x="58.42" y="144.78" smashed="yes">
<attribute name="NAME" x="55.88" y="149.86" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D58" gate="G$1" x="68.58" y="144.78" smashed="yes">
<attribute name="NAME" x="66.04" y="149.86" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D59" gate="G$1" x="78.74" y="144.78" smashed="yes">
<attribute name="NAME" x="76.2" y="149.86" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D60" gate="G$1" x="88.9" y="144.78" smashed="yes">
<attribute name="NAME" x="86.36" y="149.86" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D61" gate="G$1" x="99.06" y="144.78" smashed="yes">
<attribute name="NAME" x="96.52" y="149.86" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D62" gate="G$1" x="109.22" y="144.78" smashed="yes">
<attribute name="NAME" x="106.68" y="149.86" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D63" gate="G$1" x="119.38" y="144.78" smashed="yes">
<attribute name="NAME" x="116.84" y="149.86" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D64" gate="G$1" x="129.54" y="144.78" smashed="yes">
<attribute name="NAME" x="127" y="149.86" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D65" gate="G$1" x="139.7" y="144.78" smashed="yes">
<attribute name="NAME" x="137.16" y="149.86" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D66" gate="G$1" x="149.86" y="144.78" smashed="yes">
<attribute name="NAME" x="147.32" y="149.86" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D67" gate="G$1" x="160.02" y="144.78" smashed="yes">
<attribute name="NAME" x="157.48" y="149.86" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D68" gate="G$1" x="170.18" y="144.78" smashed="yes">
<attribute name="NAME" x="167.64" y="149.86" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D69" gate="G$1" x="180.34" y="144.78" smashed="yes">
<attribute name="NAME" x="177.8" y="149.86" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D70" gate="G$1" x="190.5" y="144.78" smashed="yes">
<attribute name="NAME" x="187.96" y="149.86" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D71" gate="G$1" x="200.66" y="144.78" smashed="yes">
<attribute name="NAME" x="198.12" y="149.86" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D72" gate="G$1" x="210.82" y="144.78" smashed="yes">
<attribute name="NAME" x="208.28" y="149.86" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D73" gate="G$1" x="220.98" y="144.78" smashed="yes">
<attribute name="NAME" x="218.44" y="149.86" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D74" gate="G$1" x="231.14" y="144.78" smashed="yes">
<attribute name="NAME" x="228.6" y="149.86" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D75" gate="G$1" x="241.3" y="144.78" smashed="yes">
<attribute name="NAME" x="238.76" y="149.86" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D76" gate="G$1" x="-2.54" y="193.04" smashed="yes">
<attribute name="NAME" x="-5.08" y="198.12" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D77" gate="G$1" x="7.62" y="193.04" smashed="yes">
<attribute name="NAME" x="5.08" y="198.12" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D78" gate="G$1" x="17.78" y="193.04" smashed="yes">
<attribute name="NAME" x="15.24" y="198.12" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D79" gate="G$1" x="27.94" y="193.04" smashed="yes">
<attribute name="NAME" x="25.4" y="198.12" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D80" gate="G$1" x="38.1" y="193.04" smashed="yes">
<attribute name="NAME" x="35.56" y="198.12" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D81" gate="G$1" x="48.26" y="193.04" smashed="yes">
<attribute name="NAME" x="45.72" y="198.12" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D82" gate="G$1" x="58.42" y="193.04" smashed="yes">
<attribute name="NAME" x="55.88" y="198.12" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D83" gate="G$1" x="68.58" y="193.04" smashed="yes">
<attribute name="NAME" x="66.04" y="198.12" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D84" gate="G$1" x="78.74" y="193.04" smashed="yes">
<attribute name="NAME" x="76.2" y="198.12" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D85" gate="G$1" x="88.9" y="193.04" smashed="yes">
<attribute name="NAME" x="86.36" y="198.12" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D86" gate="G$1" x="99.06" y="193.04" smashed="yes">
<attribute name="NAME" x="96.52" y="198.12" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D87" gate="G$1" x="109.22" y="193.04" smashed="yes">
<attribute name="NAME" x="106.68" y="198.12" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D88" gate="G$1" x="119.38" y="193.04" smashed="yes">
<attribute name="NAME" x="116.84" y="198.12" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D89" gate="G$1" x="129.54" y="193.04" smashed="yes">
<attribute name="NAME" x="127" y="198.12" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D90" gate="G$1" x="139.7" y="193.04" smashed="yes">
<attribute name="NAME" x="137.16" y="198.12" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D91" gate="G$1" x="149.86" y="193.04" smashed="yes">
<attribute name="NAME" x="147.32" y="198.12" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D92" gate="G$1" x="160.02" y="193.04" smashed="yes">
<attribute name="NAME" x="157.48" y="198.12" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D93" gate="G$1" x="170.18" y="193.04" smashed="yes">
<attribute name="NAME" x="167.64" y="198.12" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D94" gate="G$1" x="180.34" y="193.04" smashed="yes">
<attribute name="NAME" x="177.8" y="198.12" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D95" gate="G$1" x="190.5" y="193.04" smashed="yes">
<attribute name="NAME" x="187.96" y="198.12" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D96" gate="G$1" x="200.66" y="193.04" smashed="yes">
<attribute name="NAME" x="198.12" y="198.12" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D97" gate="G$1" x="210.82" y="193.04" smashed="yes">
<attribute name="NAME" x="208.28" y="198.12" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D98" gate="G$1" x="220.98" y="193.04" smashed="yes">
<attribute name="NAME" x="218.44" y="198.12" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D99" gate="G$1" x="231.14" y="193.04" smashed="yes">
<attribute name="NAME" x="228.6" y="198.12" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="D100" gate="G$1" x="241.3" y="193.04" smashed="yes">
<attribute name="NAME" x="238.76" y="198.12" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="R2" gate="G$1" x="7.62" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="5.08" y="25.4" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="10.16" y="25.4" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R3" gate="G$1" x="17.78" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="15.24" y="25.4" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="20.32" y="25.4" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R4" gate="G$1" x="27.94" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="25.4" y="25.4" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="30.48" y="25.4" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R5" gate="G$1" x="38.1" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="35.56" y="25.4" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="40.64" y="25.4" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R6" gate="G$1" x="48.26" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="45.72" y="25.4" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="50.8" y="25.4" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R7" gate="G$1" x="58.42" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="55.88" y="25.4" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="60.96" y="25.4" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R8" gate="G$1" x="68.58" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="66.04" y="25.4" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="71.12" y="25.4" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R9" gate="G$1" x="78.74" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="76.2" y="25.4" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="81.28" y="25.4" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R10" gate="G$1" x="88.9" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="86.36" y="25.4" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="91.44" y="25.4" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R11" gate="G$1" x="99.06" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="96.52" y="25.4" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="101.6" y="25.4" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R12" gate="G$1" x="109.22" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="106.68" y="25.4" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="111.76" y="25.4" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R13" gate="G$1" x="119.38" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="116.84" y="25.4" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="121.92" y="25.4" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R14" gate="G$1" x="129.54" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="127" y="25.4" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="132.08" y="25.4" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R15" gate="G$1" x="139.7" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="137.16" y="25.4" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="142.24" y="25.4" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R16" gate="G$1" x="149.86" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="147.32" y="25.4" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="152.4" y="25.4" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R17" gate="G$1" x="160.02" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="157.48" y="25.4" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="162.56" y="25.4" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R18" gate="G$1" x="170.18" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="167.64" y="25.4" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="172.72" y="25.4" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R19" gate="G$1" x="180.34" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="177.8" y="25.4" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="182.88" y="25.4" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R20" gate="G$1" x="190.5" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="187.96" y="25.4" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="193.04" y="25.4" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R21" gate="G$1" x="200.66" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="198.12" y="25.4" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="203.2" y="25.4" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R22" gate="G$1" x="210.82" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="208.28" y="25.4" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="213.36" y="25.4" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R23" gate="G$1" x="220.98" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="218.44" y="25.4" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="223.52" y="25.4" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R24" gate="G$1" x="231.14" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="228.6" y="25.4" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="233.68" y="25.4" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R25" gate="G$1" x="241.3" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="238.76" y="25.4" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="243.84" y="25.4" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R26" gate="G$1" x="-2.54" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="-5.08" y="73.66" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="0" y="73.66" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R27" gate="G$1" x="7.62" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="5.08" y="73.66" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="10.16" y="73.66" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R28" gate="G$1" x="17.78" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="15.24" y="73.66" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="20.32" y="73.66" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R29" gate="G$1" x="27.94" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="25.4" y="73.66" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="30.48" y="73.66" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R30" gate="G$1" x="38.1" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="35.56" y="73.66" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="40.64" y="73.66" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R31" gate="G$1" x="48.26" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="45.72" y="73.66" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="50.8" y="73.66" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R32" gate="G$1" x="58.42" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="55.88" y="73.66" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="60.96" y="73.66" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R33" gate="G$1" x="68.58" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="66.04" y="73.66" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="71.12" y="73.66" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R34" gate="G$1" x="78.74" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="76.2" y="73.66" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="81.28" y="73.66" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R35" gate="G$1" x="88.9" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="86.36" y="73.66" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="91.44" y="73.66" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R36" gate="G$1" x="99.06" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="96.52" y="73.66" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="101.6" y="73.66" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R37" gate="G$1" x="109.22" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="106.68" y="73.66" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="111.76" y="73.66" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R38" gate="G$1" x="119.38" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="116.84" y="73.66" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="121.92" y="73.66" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R39" gate="G$1" x="129.54" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="127" y="73.66" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="132.08" y="73.66" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R40" gate="G$1" x="139.7" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="137.16" y="73.66" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="142.24" y="73.66" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R41" gate="G$1" x="149.86" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="147.32" y="73.66" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="152.4" y="73.66" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R42" gate="G$1" x="160.02" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="157.48" y="73.66" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="162.56" y="73.66" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R43" gate="G$1" x="170.18" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="167.64" y="73.66" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="172.72" y="73.66" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R44" gate="G$1" x="180.34" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="177.8" y="73.66" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="182.88" y="73.66" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R45" gate="G$1" x="190.5" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="187.96" y="73.66" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="193.04" y="73.66" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R46" gate="G$1" x="200.66" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="198.12" y="73.66" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="203.2" y="73.66" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R47" gate="G$1" x="210.82" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="208.28" y="73.66" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="213.36" y="73.66" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R48" gate="G$1" x="220.98" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="218.44" y="73.66" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="223.52" y="73.66" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R49" gate="G$1" x="231.14" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="228.6" y="73.66" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="233.68" y="73.66" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R50" gate="G$1" x="241.3" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="238.76" y="73.66" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="243.84" y="73.66" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R51" gate="G$1" x="-2.54" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="-5.08" y="121.92" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="0" y="121.92" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R52" gate="G$1" x="7.62" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="5.08" y="121.92" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="10.16" y="121.92" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R53" gate="G$1" x="17.78" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="15.24" y="121.92" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="20.32" y="121.92" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R54" gate="G$1" x="27.94" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="25.4" y="121.92" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="30.48" y="121.92" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R55" gate="G$1" x="38.1" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="35.56" y="121.92" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="40.64" y="121.92" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R56" gate="G$1" x="48.26" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="45.72" y="121.92" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="50.8" y="121.92" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R57" gate="G$1" x="58.42" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="55.88" y="121.92" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="60.96" y="121.92" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R58" gate="G$1" x="68.58" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="66.04" y="121.92" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="71.12" y="121.92" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R59" gate="G$1" x="78.74" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="76.2" y="121.92" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="81.28" y="121.92" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R60" gate="G$1" x="88.9" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="86.36" y="121.92" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="91.44" y="121.92" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R61" gate="G$1" x="99.06" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="96.52" y="121.92" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="101.6" y="121.92" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R62" gate="G$1" x="109.22" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="106.68" y="121.92" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="111.76" y="121.92" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R63" gate="G$1" x="119.38" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="116.84" y="121.92" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="121.92" y="121.92" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R64" gate="G$1" x="129.54" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="127" y="121.92" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="132.08" y="121.92" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R65" gate="G$1" x="139.7" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="137.16" y="121.92" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="142.24" y="121.92" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R66" gate="G$1" x="149.86" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="147.32" y="121.92" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="152.4" y="121.92" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R67" gate="G$1" x="160.02" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="157.48" y="121.92" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="162.56" y="121.92" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R68" gate="G$1" x="170.18" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="167.64" y="121.92" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="172.72" y="121.92" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R69" gate="G$1" x="180.34" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="177.8" y="121.92" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="182.88" y="121.92" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R70" gate="G$1" x="190.5" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="187.96" y="121.92" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="193.04" y="121.92" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R71" gate="G$1" x="200.66" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="198.12" y="121.92" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="203.2" y="121.92" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R72" gate="G$1" x="210.82" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="208.28" y="121.92" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="213.36" y="121.92" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R73" gate="G$1" x="220.98" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="218.44" y="121.92" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="223.52" y="121.92" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R74" gate="G$1" x="231.14" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="228.6" y="121.92" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="233.68" y="121.92" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R75" gate="G$1" x="241.3" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="238.76" y="121.92" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="243.84" y="121.92" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R76" gate="G$1" x="-2.54" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="-5.08" y="170.18" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="0" y="170.18" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R77" gate="G$1" x="7.62" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="5.08" y="170.18" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="10.16" y="170.18" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R78" gate="G$1" x="17.78" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="15.24" y="170.18" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="20.32" y="170.18" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R79" gate="G$1" x="27.94" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="25.4" y="170.18" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="30.48" y="170.18" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R80" gate="G$1" x="38.1" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="35.56" y="170.18" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="40.64" y="170.18" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R81" gate="G$1" x="48.26" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="45.72" y="170.18" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="50.8" y="170.18" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R82" gate="G$1" x="58.42" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="55.88" y="170.18" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="60.96" y="170.18" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R83" gate="G$1" x="68.58" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="66.04" y="170.18" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="71.12" y="170.18" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R84" gate="G$1" x="78.74" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="76.2" y="170.18" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="81.28" y="170.18" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R85" gate="G$1" x="88.9" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="86.36" y="170.18" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="91.44" y="170.18" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R86" gate="G$1" x="99.06" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="96.52" y="170.18" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="101.6" y="170.18" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R87" gate="G$1" x="109.22" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="106.68" y="170.18" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="111.76" y="170.18" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R88" gate="G$1" x="119.38" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="116.84" y="170.18" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="121.92" y="170.18" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R89" gate="G$1" x="129.54" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="127" y="170.18" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="132.08" y="170.18" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R90" gate="G$1" x="139.7" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="137.16" y="170.18" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="142.24" y="170.18" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R91" gate="G$1" x="149.86" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="147.32" y="170.18" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="152.4" y="170.18" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R92" gate="G$1" x="160.02" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="157.48" y="170.18" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="162.56" y="170.18" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R93" gate="G$1" x="170.18" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="167.64" y="170.18" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="172.72" y="170.18" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R94" gate="G$1" x="180.34" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="177.8" y="170.18" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="182.88" y="170.18" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R95" gate="G$1" x="190.5" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="187.96" y="170.18" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="193.04" y="170.18" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R96" gate="G$1" x="200.66" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="198.12" y="170.18" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="203.2" y="170.18" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R97" gate="G$1" x="210.82" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="208.28" y="170.18" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="213.36" y="170.18" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R98" gate="G$1" x="220.98" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="218.44" y="170.18" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="223.52" y="170.18" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R99" gate="G$1" x="231.14" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="228.6" y="170.18" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="233.68" y="170.18" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R100" gate="G$1" x="241.3" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="238.76" y="170.18" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="243.84" y="170.18" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="ON" gate="G$2" x="-30.48" y="22.86" smashed="yes"/>
<instance part="ON" gate="G$1" x="-25.4" y="25.4" smashed="yes">
<attribute name="NAME" x="-27.94" y="27.94" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="USB_PWR" gate="G$1" x="-53.34" y="40.64" smashed="yes" rot="R180">
<attribute name="NAME" x="-48.26" y="29.972" size="1.778" layer="95" rot="R180"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$3" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="-2.54" y1="40.64" x2="-2.54" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="7.62" y1="30.48" x2="7.62" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="17.78" y1="30.48" x2="17.78" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="A"/>
<wire x1="27.94" y1="30.48" x2="27.94" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="D5" gate="G$1" pin="A"/>
<wire x1="38.1" y1="30.48" x2="38.1" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="D6" gate="G$1" pin="A"/>
<wire x1="48.26" y1="30.48" x2="48.26" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="D7" gate="G$1" pin="A"/>
<wire x1="58.42" y1="30.48" x2="58.42" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="D8" gate="G$1" pin="A"/>
<wire x1="68.58" y1="30.48" x2="68.58" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="D9" gate="G$1" pin="A"/>
<wire x1="78.74" y1="30.48" x2="78.74" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="2"/>
</segment>
</net>
<net name="VIN" class="0">
<segment>
<pinref part="ON" gate="G$1" pin="N"/>
<pinref part="USB_PWR" gate="G$1" pin="1_VCC"/>
<wire x1="-33.02" y1="25.4" x2="-45.72" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="25.4" x2="-45.72" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="D10" gate="G$1" pin="A"/>
<wire x1="88.9" y1="30.48" x2="88.9" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="D11" gate="G$1" pin="A"/>
<wire x1="99.06" y1="40.64" x2="99.06" y2="30.48" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="D12" gate="G$1" pin="A"/>
<wire x1="109.22" y1="30.48" x2="109.22" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="D13" gate="G$1" pin="A"/>
<wire x1="119.38" y1="40.64" x2="119.38" y2="30.48" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="D14" gate="G$1" pin="A"/>
<wire x1="129.54" y1="30.48" x2="129.54" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="D15" gate="G$1" pin="A"/>
<wire x1="139.7" y1="40.64" x2="139.7" y2="30.48" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="D16" gate="G$1" pin="A"/>
<wire x1="149.86" y1="30.48" x2="149.86" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="D17" gate="G$1" pin="A"/>
<wire x1="160.02" y1="40.64" x2="160.02" y2="30.48" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="D18" gate="G$1" pin="A"/>
<wire x1="170.18" y1="30.48" x2="170.18" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="D19" gate="G$1" pin="A"/>
<wire x1="180.34" y1="40.64" x2="180.34" y2="30.48" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="D20" gate="G$1" pin="A"/>
<wire x1="190.5" y1="30.48" x2="190.5" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="D21" gate="G$1" pin="A"/>
<wire x1="200.66" y1="40.64" x2="200.66" y2="30.48" width="0.1524" layer="91"/>
<pinref part="R21" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="D22" gate="G$1" pin="A"/>
<wire x1="210.82" y1="30.48" x2="210.82" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="D23" gate="G$1" pin="A"/>
<wire x1="220.98" y1="40.64" x2="220.98" y2="30.48" width="0.1524" layer="91"/>
<pinref part="R23" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="D24" gate="G$1" pin="A"/>
<wire x1="231.14" y1="30.48" x2="231.14" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R24" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="D25" gate="G$1" pin="A"/>
<wire x1="241.3" y1="40.64" x2="241.3" y2="30.48" width="0.1524" layer="91"/>
<pinref part="R25" gate="G$1" pin="2"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="R26" gate="G$1" pin="1"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="20.32" x2="7.62" y2="20.32" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="7.62" y1="20.32" x2="17.78" y2="20.32" width="0.1524" layer="91"/>
<junction x="7.62" y="20.32"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="17.78" y1="20.32" x2="27.94" y2="20.32" width="0.1524" layer="91"/>
<junction x="17.78" y="20.32"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="27.94" y1="20.32" x2="38.1" y2="20.32" width="0.1524" layer="91"/>
<junction x="27.94" y="20.32"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="38.1" y1="20.32" x2="48.26" y2="20.32" width="0.1524" layer="91"/>
<junction x="38.1" y="20.32"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="48.26" y1="20.32" x2="58.42" y2="20.32" width="0.1524" layer="91"/>
<junction x="48.26" y="20.32"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="58.42" y1="20.32" x2="68.58" y2="20.32" width="0.1524" layer="91"/>
<junction x="58.42" y="20.32"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="68.58" y1="20.32" x2="78.74" y2="20.32" width="0.1524" layer="91"/>
<junction x="68.58" y="20.32"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="78.74" y1="20.32" x2="88.9" y2="20.32" width="0.1524" layer="91"/>
<junction x="78.74" y="20.32"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="88.9" y1="20.32" x2="99.06" y2="20.32" width="0.1524" layer="91"/>
<junction x="88.9" y="20.32"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="99.06" y1="20.32" x2="109.22" y2="20.32" width="0.1524" layer="91"/>
<junction x="99.06" y="20.32"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="109.22" y1="20.32" x2="119.38" y2="20.32" width="0.1524" layer="91"/>
<junction x="109.22" y="20.32"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="119.38" y1="20.32" x2="129.54" y2="20.32" width="0.1524" layer="91"/>
<junction x="119.38" y="20.32"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="129.54" y1="20.32" x2="139.7" y2="20.32" width="0.1524" layer="91"/>
<junction x="129.54" y="20.32"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="139.7" y1="20.32" x2="149.86" y2="20.32" width="0.1524" layer="91"/>
<junction x="139.7" y="20.32"/>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="149.86" y1="20.32" x2="160.02" y2="20.32" width="0.1524" layer="91"/>
<junction x="149.86" y="20.32"/>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="160.02" y1="20.32" x2="170.18" y2="20.32" width="0.1524" layer="91"/>
<junction x="160.02" y="20.32"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="170.18" y1="20.32" x2="180.34" y2="20.32" width="0.1524" layer="91"/>
<junction x="170.18" y="20.32"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="180.34" y1="20.32" x2="190.5" y2="20.32" width="0.1524" layer="91"/>
<junction x="180.34" y="20.32"/>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="190.5" y1="20.32" x2="200.66" y2="20.32" width="0.1524" layer="91"/>
<junction x="190.5" y="20.32"/>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="200.66" y1="20.32" x2="210.82" y2="20.32" width="0.1524" layer="91"/>
<junction x="200.66" y="20.32"/>
<pinref part="R23" gate="G$1" pin="1"/>
<wire x1="210.82" y1="20.32" x2="220.98" y2="20.32" width="0.1524" layer="91"/>
<junction x="210.82" y="20.32"/>
<pinref part="R24" gate="G$1" pin="1"/>
<wire x1="220.98" y1="20.32" x2="231.14" y2="20.32" width="0.1524" layer="91"/>
<junction x="220.98" y="20.32"/>
<pinref part="R25" gate="G$1" pin="1"/>
<wire x1="231.14" y1="20.32" x2="241.3" y2="20.32" width="0.1524" layer="91"/>
<junction x="231.14" y="20.32"/>
<pinref part="R27" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="68.58" x2="7.62" y2="68.58" width="0.1524" layer="91"/>
<pinref part="R28" gate="G$1" pin="1"/>
<wire x1="7.62" y1="68.58" x2="17.78" y2="68.58" width="0.1524" layer="91"/>
<junction x="7.62" y="68.58"/>
<pinref part="R29" gate="G$1" pin="1"/>
<wire x1="17.78" y1="68.58" x2="27.94" y2="68.58" width="0.1524" layer="91"/>
<junction x="17.78" y="68.58"/>
<pinref part="R30" gate="G$1" pin="1"/>
<wire x1="27.94" y1="68.58" x2="38.1" y2="68.58" width="0.1524" layer="91"/>
<junction x="27.94" y="68.58"/>
<pinref part="R31" gate="G$1" pin="1"/>
<wire x1="38.1" y1="68.58" x2="48.26" y2="68.58" width="0.1524" layer="91"/>
<junction x="38.1" y="68.58"/>
<pinref part="R32" gate="G$1" pin="1"/>
<wire x1="48.26" y1="68.58" x2="58.42" y2="68.58" width="0.1524" layer="91"/>
<junction x="48.26" y="68.58"/>
<pinref part="R33" gate="G$1" pin="1"/>
<wire x1="58.42" y1="68.58" x2="68.58" y2="68.58" width="0.1524" layer="91"/>
<junction x="58.42" y="68.58"/>
<pinref part="R34" gate="G$1" pin="1"/>
<wire x1="68.58" y1="68.58" x2="78.74" y2="68.58" width="0.1524" layer="91"/>
<junction x="68.58" y="68.58"/>
<pinref part="R35" gate="G$1" pin="1"/>
<wire x1="78.74" y1="68.58" x2="88.9" y2="68.58" width="0.1524" layer="91"/>
<junction x="78.74" y="68.58"/>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="88.9" y1="68.58" x2="99.06" y2="68.58" width="0.1524" layer="91"/>
<junction x="88.9" y="68.58"/>
<pinref part="R37" gate="G$1" pin="1"/>
<wire x1="99.06" y1="68.58" x2="109.22" y2="68.58" width="0.1524" layer="91"/>
<junction x="99.06" y="68.58"/>
<pinref part="R38" gate="G$1" pin="1"/>
<wire x1="109.22" y1="68.58" x2="119.38" y2="68.58" width="0.1524" layer="91"/>
<junction x="109.22" y="68.58"/>
<pinref part="R39" gate="G$1" pin="1"/>
<wire x1="119.38" y1="68.58" x2="129.54" y2="68.58" width="0.1524" layer="91"/>
<junction x="119.38" y="68.58"/>
<pinref part="R40" gate="G$1" pin="1"/>
<wire x1="129.54" y1="68.58" x2="139.7" y2="68.58" width="0.1524" layer="91"/>
<junction x="129.54" y="68.58"/>
<pinref part="R41" gate="G$1" pin="1"/>
<wire x1="139.7" y1="68.58" x2="149.86" y2="68.58" width="0.1524" layer="91"/>
<junction x="139.7" y="68.58"/>
<pinref part="R42" gate="G$1" pin="1"/>
<wire x1="149.86" y1="68.58" x2="160.02" y2="68.58" width="0.1524" layer="91"/>
<junction x="149.86" y="68.58"/>
<pinref part="R43" gate="G$1" pin="1"/>
<wire x1="160.02" y1="68.58" x2="170.18" y2="68.58" width="0.1524" layer="91"/>
<junction x="160.02" y="68.58"/>
<pinref part="R44" gate="G$1" pin="1"/>
<wire x1="170.18" y1="68.58" x2="180.34" y2="68.58" width="0.1524" layer="91"/>
<junction x="170.18" y="68.58"/>
<pinref part="R45" gate="G$1" pin="1"/>
<wire x1="180.34" y1="68.58" x2="190.5" y2="68.58" width="0.1524" layer="91"/>
<junction x="180.34" y="68.58"/>
<pinref part="R46" gate="G$1" pin="1"/>
<wire x1="190.5" y1="68.58" x2="200.66" y2="68.58" width="0.1524" layer="91"/>
<junction x="190.5" y="68.58"/>
<pinref part="R47" gate="G$1" pin="1"/>
<wire x1="200.66" y1="68.58" x2="210.82" y2="68.58" width="0.1524" layer="91"/>
<junction x="200.66" y="68.58"/>
<pinref part="R48" gate="G$1" pin="1"/>
<wire x1="210.82" y1="68.58" x2="220.98" y2="68.58" width="0.1524" layer="91"/>
<junction x="210.82" y="68.58"/>
<pinref part="R49" gate="G$1" pin="1"/>
<wire x1="220.98" y1="68.58" x2="231.14" y2="68.58" width="0.1524" layer="91"/>
<junction x="220.98" y="68.58"/>
<pinref part="R50" gate="G$1" pin="1"/>
<wire x1="231.14" y1="68.58" x2="241.3" y2="68.58" width="0.1524" layer="91"/>
<junction x="231.14" y="68.58"/>
<pinref part="R51" gate="G$1" pin="1"/>
<pinref part="R52" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="116.84" x2="7.62" y2="116.84" width="0.1524" layer="91"/>
<pinref part="R53" gate="G$1" pin="1"/>
<wire x1="7.62" y1="116.84" x2="17.78" y2="116.84" width="0.1524" layer="91"/>
<junction x="7.62" y="116.84"/>
<pinref part="R54" gate="G$1" pin="1"/>
<wire x1="17.78" y1="116.84" x2="27.94" y2="116.84" width="0.1524" layer="91"/>
<junction x="17.78" y="116.84"/>
<pinref part="R55" gate="G$1" pin="1"/>
<wire x1="27.94" y1="116.84" x2="38.1" y2="116.84" width="0.1524" layer="91"/>
<junction x="27.94" y="116.84"/>
<pinref part="R56" gate="G$1" pin="1"/>
<wire x1="38.1" y1="116.84" x2="48.26" y2="116.84" width="0.1524" layer="91"/>
<junction x="38.1" y="116.84"/>
<pinref part="R57" gate="G$1" pin="1"/>
<wire x1="48.26" y1="116.84" x2="58.42" y2="116.84" width="0.1524" layer="91"/>
<junction x="48.26" y="116.84"/>
<pinref part="R58" gate="G$1" pin="1"/>
<wire x1="58.42" y1="116.84" x2="68.58" y2="116.84" width="0.1524" layer="91"/>
<junction x="58.42" y="116.84"/>
<pinref part="R59" gate="G$1" pin="1"/>
<wire x1="68.58" y1="116.84" x2="78.74" y2="116.84" width="0.1524" layer="91"/>
<junction x="68.58" y="116.84"/>
<pinref part="R60" gate="G$1" pin="1"/>
<wire x1="78.74" y1="116.84" x2="88.9" y2="116.84" width="0.1524" layer="91"/>
<junction x="78.74" y="116.84"/>
<pinref part="R61" gate="G$1" pin="1"/>
<wire x1="88.9" y1="116.84" x2="99.06" y2="116.84" width="0.1524" layer="91"/>
<junction x="88.9" y="116.84"/>
<pinref part="R62" gate="G$1" pin="1"/>
<wire x1="99.06" y1="116.84" x2="109.22" y2="116.84" width="0.1524" layer="91"/>
<junction x="99.06" y="116.84"/>
<pinref part="R63" gate="G$1" pin="1"/>
<wire x1="109.22" y1="116.84" x2="119.38" y2="116.84" width="0.1524" layer="91"/>
<junction x="109.22" y="116.84"/>
<pinref part="R64" gate="G$1" pin="1"/>
<wire x1="119.38" y1="116.84" x2="129.54" y2="116.84" width="0.1524" layer="91"/>
<junction x="119.38" y="116.84"/>
<pinref part="R65" gate="G$1" pin="1"/>
<wire x1="129.54" y1="116.84" x2="139.7" y2="116.84" width="0.1524" layer="91"/>
<junction x="129.54" y="116.84"/>
<pinref part="R66" gate="G$1" pin="1"/>
<wire x1="139.7" y1="116.84" x2="149.86" y2="116.84" width="0.1524" layer="91"/>
<junction x="139.7" y="116.84"/>
<pinref part="R67" gate="G$1" pin="1"/>
<wire x1="149.86" y1="116.84" x2="160.02" y2="116.84" width="0.1524" layer="91"/>
<junction x="149.86" y="116.84"/>
<pinref part="R68" gate="G$1" pin="1"/>
<wire x1="160.02" y1="116.84" x2="170.18" y2="116.84" width="0.1524" layer="91"/>
<junction x="160.02" y="116.84"/>
<pinref part="R69" gate="G$1" pin="1"/>
<wire x1="170.18" y1="116.84" x2="180.34" y2="116.84" width="0.1524" layer="91"/>
<junction x="170.18" y="116.84"/>
<pinref part="R70" gate="G$1" pin="1"/>
<wire x1="180.34" y1="116.84" x2="190.5" y2="116.84" width="0.1524" layer="91"/>
<junction x="180.34" y="116.84"/>
<pinref part="R71" gate="G$1" pin="1"/>
<wire x1="190.5" y1="116.84" x2="200.66" y2="116.84" width="0.1524" layer="91"/>
<junction x="190.5" y="116.84"/>
<pinref part="R72" gate="G$1" pin="1"/>
<wire x1="200.66" y1="116.84" x2="210.82" y2="116.84" width="0.1524" layer="91"/>
<junction x="200.66" y="116.84"/>
<pinref part="R73" gate="G$1" pin="1"/>
<wire x1="210.82" y1="116.84" x2="220.98" y2="116.84" width="0.1524" layer="91"/>
<junction x="210.82" y="116.84"/>
<pinref part="R74" gate="G$1" pin="1"/>
<wire x1="220.98" y1="116.84" x2="231.14" y2="116.84" width="0.1524" layer="91"/>
<junction x="220.98" y="116.84"/>
<pinref part="R75" gate="G$1" pin="1"/>
<wire x1="231.14" y1="116.84" x2="241.3" y2="116.84" width="0.1524" layer="91"/>
<junction x="231.14" y="116.84"/>
<wire x1="241.3" y1="20.32" x2="264.16" y2="20.32" width="0.1524" layer="91"/>
<junction x="241.3" y="20.32"/>
<wire x1="264.16" y1="20.32" x2="264.16" y2="68.58" width="0.1524" layer="91"/>
<wire x1="264.16" y1="68.58" x2="241.3" y2="68.58" width="0.1524" layer="91"/>
<junction x="241.3" y="68.58"/>
<wire x1="264.16" y1="68.58" x2="264.16" y2="116.84" width="0.1524" layer="91"/>
<wire x1="264.16" y1="116.84" x2="241.3" y2="116.84" width="0.1524" layer="91"/>
<junction x="264.16" y="68.58"/>
<junction x="241.3" y="116.84"/>
<pinref part="R100" gate="G$1" pin="1"/>
<wire x1="264.16" y1="116.84" x2="264.16" y2="165.1" width="0.1524" layer="91"/>
<wire x1="264.16" y1="165.1" x2="241.3" y2="165.1" width="0.1524" layer="91"/>
<junction x="264.16" y="116.84"/>
<pinref part="R99" gate="G$1" pin="1"/>
<wire x1="241.3" y1="165.1" x2="231.14" y2="165.1" width="0.1524" layer="91"/>
<junction x="241.3" y="165.1"/>
<pinref part="R98" gate="G$1" pin="1"/>
<wire x1="231.14" y1="165.1" x2="220.98" y2="165.1" width="0.1524" layer="91"/>
<junction x="231.14" y="165.1"/>
<pinref part="R97" gate="G$1" pin="1"/>
<wire x1="220.98" y1="165.1" x2="210.82" y2="165.1" width="0.1524" layer="91"/>
<junction x="220.98" y="165.1"/>
<pinref part="R96" gate="G$1" pin="1"/>
<wire x1="210.82" y1="165.1" x2="200.66" y2="165.1" width="0.1524" layer="91"/>
<junction x="210.82" y="165.1"/>
<pinref part="R95" gate="G$1" pin="1"/>
<wire x1="200.66" y1="165.1" x2="190.5" y2="165.1" width="0.1524" layer="91"/>
<junction x="200.66" y="165.1"/>
<pinref part="R94" gate="G$1" pin="1"/>
<wire x1="190.5" y1="165.1" x2="180.34" y2="165.1" width="0.1524" layer="91"/>
<junction x="190.5" y="165.1"/>
<pinref part="R93" gate="G$1" pin="1"/>
<wire x1="180.34" y1="165.1" x2="170.18" y2="165.1" width="0.1524" layer="91"/>
<junction x="180.34" y="165.1"/>
<pinref part="R92" gate="G$1" pin="1"/>
<wire x1="170.18" y1="165.1" x2="160.02" y2="165.1" width="0.1524" layer="91"/>
<junction x="170.18" y="165.1"/>
<pinref part="R91" gate="G$1" pin="1"/>
<wire x1="160.02" y1="165.1" x2="149.86" y2="165.1" width="0.1524" layer="91"/>
<junction x="160.02" y="165.1"/>
<pinref part="R90" gate="G$1" pin="1"/>
<wire x1="149.86" y1="165.1" x2="139.7" y2="165.1" width="0.1524" layer="91"/>
<junction x="149.86" y="165.1"/>
<pinref part="R89" gate="G$1" pin="1"/>
<wire x1="139.7" y1="165.1" x2="129.54" y2="165.1" width="0.1524" layer="91"/>
<junction x="139.7" y="165.1"/>
<pinref part="R88" gate="G$1" pin="1"/>
<wire x1="129.54" y1="165.1" x2="119.38" y2="165.1" width="0.1524" layer="91"/>
<junction x="129.54" y="165.1"/>
<pinref part="R87" gate="G$1" pin="1"/>
<wire x1="119.38" y1="165.1" x2="109.22" y2="165.1" width="0.1524" layer="91"/>
<junction x="119.38" y="165.1"/>
<pinref part="R86" gate="G$1" pin="1"/>
<wire x1="109.22" y1="165.1" x2="99.06" y2="165.1" width="0.1524" layer="91"/>
<junction x="109.22" y="165.1"/>
<pinref part="R85" gate="G$1" pin="1"/>
<wire x1="99.06" y1="165.1" x2="88.9" y2="165.1" width="0.1524" layer="91"/>
<junction x="99.06" y="165.1"/>
<pinref part="R84" gate="G$1" pin="1"/>
<wire x1="88.9" y1="165.1" x2="78.74" y2="165.1" width="0.1524" layer="91"/>
<junction x="88.9" y="165.1"/>
<pinref part="R83" gate="G$1" pin="1"/>
<wire x1="78.74" y1="165.1" x2="68.58" y2="165.1" width="0.1524" layer="91"/>
<junction x="78.74" y="165.1"/>
<pinref part="R82" gate="G$1" pin="1"/>
<wire x1="68.58" y1="165.1" x2="58.42" y2="165.1" width="0.1524" layer="91"/>
<junction x="68.58" y="165.1"/>
<pinref part="R81" gate="G$1" pin="1"/>
<wire x1="58.42" y1="165.1" x2="48.26" y2="165.1" width="0.1524" layer="91"/>
<junction x="58.42" y="165.1"/>
<pinref part="R80" gate="G$1" pin="1"/>
<wire x1="48.26" y1="165.1" x2="38.1" y2="165.1" width="0.1524" layer="91"/>
<junction x="48.26" y="165.1"/>
<pinref part="R79" gate="G$1" pin="1"/>
<wire x1="38.1" y1="165.1" x2="27.94" y2="165.1" width="0.1524" layer="91"/>
<junction x="38.1" y="165.1"/>
<pinref part="R78" gate="G$1" pin="1"/>
<wire x1="27.94" y1="165.1" x2="17.78" y2="165.1" width="0.1524" layer="91"/>
<junction x="27.94" y="165.1"/>
<pinref part="R77" gate="G$1" pin="1"/>
<wire x1="17.78" y1="165.1" x2="7.62" y2="165.1" width="0.1524" layer="91"/>
<junction x="17.78" y="165.1"/>
<pinref part="R76" gate="G$1" pin="1"/>
<wire x1="7.62" y1="165.1" x2="-2.54" y2="165.1" width="0.1524" layer="91"/>
<junction x="7.62" y="165.1"/>
<pinref part="ON" gate="G$1" pin="B"/>
<wire x1="-17.78" y1="20.32" x2="-2.54" y2="20.32" width="0.1524" layer="91"/>
<junction x="-2.54" y="20.32"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="D25" gate="G$1" pin="C"/>
<pinref part="D24" gate="G$1" pin="C"/>
<wire x1="231.14" y1="55.88" x2="241.3" y2="55.88" width="0.1524" layer="91"/>
<junction x="231.14" y="55.88"/>
<pinref part="D23" gate="G$1" pin="C"/>
<wire x1="220.98" y1="55.88" x2="231.14" y2="55.88" width="0.1524" layer="91"/>
<junction x="220.98" y="55.88"/>
<pinref part="D22" gate="G$1" pin="C"/>
<wire x1="210.82" y1="55.88" x2="220.98" y2="55.88" width="0.1524" layer="91"/>
<junction x="210.82" y="55.88"/>
<pinref part="D21" gate="G$1" pin="C"/>
<pinref part="D20" gate="G$1" pin="C"/>
<pinref part="D19" gate="G$1" pin="C"/>
<pinref part="D18" gate="G$1" pin="C"/>
<pinref part="D17" gate="G$1" pin="C"/>
<pinref part="D16" gate="G$1" pin="C"/>
<pinref part="D15" gate="G$1" pin="C"/>
<pinref part="D14" gate="G$1" pin="C"/>
<pinref part="D13" gate="G$1" pin="C"/>
<pinref part="D12" gate="G$1" pin="C"/>
<pinref part="D11" gate="G$1" pin="C"/>
<pinref part="D10" gate="G$1" pin="C"/>
<pinref part="D9" gate="G$1" pin="C"/>
<pinref part="D8" gate="G$1" pin="C"/>
<pinref part="D7" gate="G$1" pin="C"/>
<pinref part="D6" gate="G$1" pin="C"/>
<pinref part="D5" gate="G$1" pin="C"/>
<pinref part="D4" gate="G$1" pin="C"/>
<pinref part="D3" gate="G$1" pin="C"/>
<pinref part="D2" gate="G$1" pin="C"/>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="-2.54" y1="55.88" x2="-27.94" y2="55.88" width="0.1524" layer="91"/>
<junction x="-2.54" y="55.88"/>
<wire x1="-2.54" y1="55.88" x2="7.62" y2="55.88" width="0.1524" layer="91"/>
<junction x="7.62" y="55.88"/>
<wire x1="7.62" y1="55.88" x2="17.78" y2="55.88" width="0.1524" layer="91"/>
<junction x="17.78" y="55.88"/>
<wire x1="17.78" y1="55.88" x2="27.94" y2="55.88" width="0.1524" layer="91"/>
<junction x="27.94" y="55.88"/>
<wire x1="27.94" y1="55.88" x2="38.1" y2="55.88" width="0.1524" layer="91"/>
<junction x="38.1" y="55.88"/>
<wire x1="38.1" y1="55.88" x2="48.26" y2="55.88" width="0.1524" layer="91"/>
<junction x="48.26" y="55.88"/>
<wire x1="48.26" y1="55.88" x2="58.42" y2="55.88" width="0.1524" layer="91"/>
<junction x="58.42" y="55.88"/>
<wire x1="58.42" y1="55.88" x2="68.58" y2="55.88" width="0.1524" layer="91"/>
<junction x="68.58" y="55.88"/>
<wire x1="68.58" y1="55.88" x2="78.74" y2="55.88" width="0.1524" layer="91"/>
<junction x="78.74" y="55.88"/>
<wire x1="78.74" y1="55.88" x2="88.9" y2="55.88" width="0.1524" layer="91"/>
<junction x="88.9" y="55.88"/>
<wire x1="88.9" y1="55.88" x2="99.06" y2="55.88" width="0.1524" layer="91"/>
<junction x="99.06" y="55.88"/>
<wire x1="99.06" y1="55.88" x2="109.22" y2="55.88" width="0.1524" layer="91"/>
<junction x="109.22" y="55.88"/>
<wire x1="109.22" y1="55.88" x2="119.38" y2="55.88" width="0.1524" layer="91"/>
<junction x="119.38" y="55.88"/>
<wire x1="119.38" y1="55.88" x2="129.54" y2="55.88" width="0.1524" layer="91"/>
<junction x="129.54" y="55.88"/>
<wire x1="129.54" y1="55.88" x2="139.7" y2="55.88" width="0.1524" layer="91"/>
<junction x="139.7" y="55.88"/>
<wire x1="139.7" y1="55.88" x2="149.86" y2="55.88" width="0.1524" layer="91"/>
<junction x="149.86" y="55.88"/>
<wire x1="149.86" y1="55.88" x2="160.02" y2="55.88" width="0.1524" layer="91"/>
<junction x="160.02" y="55.88"/>
<wire x1="160.02" y1="55.88" x2="170.18" y2="55.88" width="0.1524" layer="91"/>
<junction x="170.18" y="55.88"/>
<wire x1="170.18" y1="55.88" x2="180.34" y2="55.88" width="0.1524" layer="91"/>
<junction x="180.34" y="55.88"/>
<wire x1="180.34" y1="55.88" x2="190.5" y2="55.88" width="0.1524" layer="91"/>
<junction x="190.5" y="55.88"/>
<wire x1="190.5" y1="55.88" x2="200.66" y2="55.88" width="0.1524" layer="91"/>
<wire x1="200.66" y1="55.88" x2="210.82" y2="55.88" width="0.1524" layer="91"/>
<junction x="200.66" y="55.88"/>
<pinref part="D50" gate="G$1" pin="C"/>
<pinref part="D49" gate="G$1" pin="C"/>
<wire x1="231.14" y1="104.14" x2="241.3" y2="104.14" width="0.1524" layer="91"/>
<junction x="231.14" y="104.14"/>
<pinref part="D48" gate="G$1" pin="C"/>
<wire x1="220.98" y1="104.14" x2="231.14" y2="104.14" width="0.1524" layer="91"/>
<junction x="220.98" y="104.14"/>
<pinref part="D47" gate="G$1" pin="C"/>
<wire x1="210.82" y1="104.14" x2="220.98" y2="104.14" width="0.1524" layer="91"/>
<junction x="210.82" y="104.14"/>
<pinref part="D46" gate="G$1" pin="C"/>
<pinref part="D45" gate="G$1" pin="C"/>
<pinref part="D44" gate="G$1" pin="C"/>
<pinref part="D43" gate="G$1" pin="C"/>
<pinref part="D42" gate="G$1" pin="C"/>
<pinref part="D41" gate="G$1" pin="C"/>
<pinref part="D40" gate="G$1" pin="C"/>
<pinref part="D39" gate="G$1" pin="C"/>
<pinref part="D38" gate="G$1" pin="C"/>
<pinref part="D37" gate="G$1" pin="C"/>
<pinref part="D36" gate="G$1" pin="C"/>
<pinref part="D35" gate="G$1" pin="C"/>
<pinref part="D34" gate="G$1" pin="C"/>
<pinref part="D33" gate="G$1" pin="C"/>
<pinref part="D32" gate="G$1" pin="C"/>
<pinref part="D31" gate="G$1" pin="C"/>
<pinref part="D30" gate="G$1" pin="C"/>
<pinref part="D29" gate="G$1" pin="C"/>
<pinref part="D28" gate="G$1" pin="C"/>
<pinref part="D27" gate="G$1" pin="C"/>
<pinref part="D26" gate="G$1" pin="C"/>
<wire x1="-2.54" y1="104.14" x2="-27.94" y2="104.14" width="0.1524" layer="91"/>
<junction x="-2.54" y="104.14"/>
<wire x1="-2.54" y1="104.14" x2="7.62" y2="104.14" width="0.1524" layer="91"/>
<junction x="7.62" y="104.14"/>
<wire x1="7.62" y1="104.14" x2="17.78" y2="104.14" width="0.1524" layer="91"/>
<junction x="17.78" y="104.14"/>
<wire x1="17.78" y1="104.14" x2="27.94" y2="104.14" width="0.1524" layer="91"/>
<junction x="27.94" y="104.14"/>
<wire x1="27.94" y1="104.14" x2="38.1" y2="104.14" width="0.1524" layer="91"/>
<junction x="38.1" y="104.14"/>
<wire x1="38.1" y1="104.14" x2="48.26" y2="104.14" width="0.1524" layer="91"/>
<junction x="48.26" y="104.14"/>
<wire x1="48.26" y1="104.14" x2="58.42" y2="104.14" width="0.1524" layer="91"/>
<junction x="58.42" y="104.14"/>
<wire x1="58.42" y1="104.14" x2="68.58" y2="104.14" width="0.1524" layer="91"/>
<junction x="68.58" y="104.14"/>
<wire x1="68.58" y1="104.14" x2="78.74" y2="104.14" width="0.1524" layer="91"/>
<junction x="78.74" y="104.14"/>
<wire x1="78.74" y1="104.14" x2="88.9" y2="104.14" width="0.1524" layer="91"/>
<junction x="88.9" y="104.14"/>
<wire x1="88.9" y1="104.14" x2="99.06" y2="104.14" width="0.1524" layer="91"/>
<junction x="99.06" y="104.14"/>
<wire x1="99.06" y1="104.14" x2="109.22" y2="104.14" width="0.1524" layer="91"/>
<junction x="109.22" y="104.14"/>
<wire x1="109.22" y1="104.14" x2="119.38" y2="104.14" width="0.1524" layer="91"/>
<junction x="119.38" y="104.14"/>
<wire x1="119.38" y1="104.14" x2="129.54" y2="104.14" width="0.1524" layer="91"/>
<junction x="129.54" y="104.14"/>
<wire x1="129.54" y1="104.14" x2="139.7" y2="104.14" width="0.1524" layer="91"/>
<junction x="139.7" y="104.14"/>
<wire x1="139.7" y1="104.14" x2="149.86" y2="104.14" width="0.1524" layer="91"/>
<junction x="149.86" y="104.14"/>
<wire x1="149.86" y1="104.14" x2="160.02" y2="104.14" width="0.1524" layer="91"/>
<junction x="160.02" y="104.14"/>
<wire x1="160.02" y1="104.14" x2="170.18" y2="104.14" width="0.1524" layer="91"/>
<junction x="170.18" y="104.14"/>
<wire x1="170.18" y1="104.14" x2="180.34" y2="104.14" width="0.1524" layer="91"/>
<junction x="180.34" y="104.14"/>
<wire x1="180.34" y1="104.14" x2="190.5" y2="104.14" width="0.1524" layer="91"/>
<junction x="190.5" y="104.14"/>
<wire x1="190.5" y1="104.14" x2="200.66" y2="104.14" width="0.1524" layer="91"/>
<wire x1="200.66" y1="104.14" x2="210.82" y2="104.14" width="0.1524" layer="91"/>
<junction x="200.66" y="104.14"/>
<wire x1="-27.94" y1="55.88" x2="-27.94" y2="104.14" width="0.1524" layer="91"/>
<junction x="-27.94" y="55.88"/>
<pinref part="D75" gate="G$1" pin="C"/>
<pinref part="D74" gate="G$1" pin="C"/>
<wire x1="231.14" y1="152.4" x2="241.3" y2="152.4" width="0.1524" layer="91"/>
<junction x="231.14" y="152.4"/>
<pinref part="D73" gate="G$1" pin="C"/>
<wire x1="220.98" y1="152.4" x2="231.14" y2="152.4" width="0.1524" layer="91"/>
<junction x="220.98" y="152.4"/>
<pinref part="D72" gate="G$1" pin="C"/>
<wire x1="210.82" y1="152.4" x2="220.98" y2="152.4" width="0.1524" layer="91"/>
<junction x="210.82" y="152.4"/>
<pinref part="D71" gate="G$1" pin="C"/>
<pinref part="D70" gate="G$1" pin="C"/>
<pinref part="D69" gate="G$1" pin="C"/>
<pinref part="D68" gate="G$1" pin="C"/>
<pinref part="D67" gate="G$1" pin="C"/>
<pinref part="D66" gate="G$1" pin="C"/>
<pinref part="D65" gate="G$1" pin="C"/>
<pinref part="D64" gate="G$1" pin="C"/>
<pinref part="D63" gate="G$1" pin="C"/>
<pinref part="D62" gate="G$1" pin="C"/>
<pinref part="D61" gate="G$1" pin="C"/>
<pinref part="D60" gate="G$1" pin="C"/>
<pinref part="D59" gate="G$1" pin="C"/>
<pinref part="D58" gate="G$1" pin="C"/>
<pinref part="D57" gate="G$1" pin="C"/>
<pinref part="D56" gate="G$1" pin="C"/>
<pinref part="D55" gate="G$1" pin="C"/>
<pinref part="D54" gate="G$1" pin="C"/>
<pinref part="D53" gate="G$1" pin="C"/>
<pinref part="D52" gate="G$1" pin="C"/>
<pinref part="D51" gate="G$1" pin="C"/>
<wire x1="-2.54" y1="152.4" x2="-27.94" y2="152.4" width="0.1524" layer="91"/>
<junction x="-2.54" y="152.4"/>
<wire x1="-2.54" y1="152.4" x2="7.62" y2="152.4" width="0.1524" layer="91"/>
<junction x="7.62" y="152.4"/>
<wire x1="7.62" y1="152.4" x2="17.78" y2="152.4" width="0.1524" layer="91"/>
<junction x="17.78" y="152.4"/>
<wire x1="17.78" y1="152.4" x2="27.94" y2="152.4" width="0.1524" layer="91"/>
<junction x="27.94" y="152.4"/>
<wire x1="27.94" y1="152.4" x2="38.1" y2="152.4" width="0.1524" layer="91"/>
<junction x="38.1" y="152.4"/>
<wire x1="38.1" y1="152.4" x2="48.26" y2="152.4" width="0.1524" layer="91"/>
<junction x="48.26" y="152.4"/>
<wire x1="48.26" y1="152.4" x2="58.42" y2="152.4" width="0.1524" layer="91"/>
<junction x="58.42" y="152.4"/>
<wire x1="58.42" y1="152.4" x2="68.58" y2="152.4" width="0.1524" layer="91"/>
<junction x="68.58" y="152.4"/>
<wire x1="68.58" y1="152.4" x2="78.74" y2="152.4" width="0.1524" layer="91"/>
<junction x="78.74" y="152.4"/>
<wire x1="78.74" y1="152.4" x2="88.9" y2="152.4" width="0.1524" layer="91"/>
<junction x="88.9" y="152.4"/>
<wire x1="88.9" y1="152.4" x2="99.06" y2="152.4" width="0.1524" layer="91"/>
<junction x="99.06" y="152.4"/>
<wire x1="99.06" y1="152.4" x2="109.22" y2="152.4" width="0.1524" layer="91"/>
<junction x="109.22" y="152.4"/>
<wire x1="109.22" y1="152.4" x2="119.38" y2="152.4" width="0.1524" layer="91"/>
<junction x="119.38" y="152.4"/>
<wire x1="119.38" y1="152.4" x2="129.54" y2="152.4" width="0.1524" layer="91"/>
<junction x="129.54" y="152.4"/>
<wire x1="129.54" y1="152.4" x2="139.7" y2="152.4" width="0.1524" layer="91"/>
<junction x="139.7" y="152.4"/>
<wire x1="139.7" y1="152.4" x2="149.86" y2="152.4" width="0.1524" layer="91"/>
<junction x="149.86" y="152.4"/>
<wire x1="149.86" y1="152.4" x2="160.02" y2="152.4" width="0.1524" layer="91"/>
<junction x="160.02" y="152.4"/>
<wire x1="160.02" y1="152.4" x2="170.18" y2="152.4" width="0.1524" layer="91"/>
<junction x="170.18" y="152.4"/>
<wire x1="170.18" y1="152.4" x2="180.34" y2="152.4" width="0.1524" layer="91"/>
<junction x="180.34" y="152.4"/>
<wire x1="180.34" y1="152.4" x2="190.5" y2="152.4" width="0.1524" layer="91"/>
<junction x="190.5" y="152.4"/>
<wire x1="190.5" y1="152.4" x2="200.66" y2="152.4" width="0.1524" layer="91"/>
<wire x1="200.66" y1="152.4" x2="210.82" y2="152.4" width="0.1524" layer="91"/>
<junction x="200.66" y="152.4"/>
<wire x1="-27.94" y1="104.14" x2="-27.94" y2="152.4" width="0.1524" layer="91"/>
<junction x="-27.94" y="104.14"/>
<pinref part="D100" gate="G$1" pin="C"/>
<pinref part="D99" gate="G$1" pin="C"/>
<wire x1="231.14" y1="200.66" x2="241.3" y2="200.66" width="0.1524" layer="91"/>
<junction x="231.14" y="200.66"/>
<pinref part="D98" gate="G$1" pin="C"/>
<wire x1="220.98" y1="200.66" x2="231.14" y2="200.66" width="0.1524" layer="91"/>
<junction x="220.98" y="200.66"/>
<pinref part="D97" gate="G$1" pin="C"/>
<wire x1="210.82" y1="200.66" x2="220.98" y2="200.66" width="0.1524" layer="91"/>
<junction x="210.82" y="200.66"/>
<pinref part="D96" gate="G$1" pin="C"/>
<wire x1="200.66" y1="200.66" x2="210.82" y2="200.66" width="0.1524" layer="91"/>
<junction x="200.66" y="200.66"/>
<pinref part="D95" gate="G$1" pin="C"/>
<junction x="190.5" y="200.66"/>
<wire x1="190.5" y1="200.66" x2="200.66" y2="200.66" width="0.1524" layer="91"/>
<pinref part="D94" gate="G$1" pin="C"/>
<junction x="180.34" y="200.66"/>
<wire x1="180.34" y1="200.66" x2="190.5" y2="200.66" width="0.1524" layer="91"/>
<pinref part="D93" gate="G$1" pin="C"/>
<junction x="170.18" y="200.66"/>
<wire x1="170.18" y1="200.66" x2="180.34" y2="200.66" width="0.1524" layer="91"/>
<pinref part="D92" gate="G$1" pin="C"/>
<junction x="160.02" y="200.66"/>
<wire x1="160.02" y1="200.66" x2="170.18" y2="200.66" width="0.1524" layer="91"/>
<pinref part="D91" gate="G$1" pin="C"/>
<junction x="149.86" y="200.66"/>
<wire x1="149.86" y1="200.66" x2="160.02" y2="200.66" width="0.1524" layer="91"/>
<pinref part="D90" gate="G$1" pin="C"/>
<junction x="139.7" y="200.66"/>
<wire x1="139.7" y1="200.66" x2="149.86" y2="200.66" width="0.1524" layer="91"/>
<pinref part="D89" gate="G$1" pin="C"/>
<junction x="129.54" y="200.66"/>
<wire x1="129.54" y1="200.66" x2="139.7" y2="200.66" width="0.1524" layer="91"/>
<pinref part="D88" gate="G$1" pin="C"/>
<junction x="119.38" y="200.66"/>
<wire x1="119.38" y1="200.66" x2="129.54" y2="200.66" width="0.1524" layer="91"/>
<pinref part="D87" gate="G$1" pin="C"/>
<junction x="109.22" y="200.66"/>
<wire x1="109.22" y1="200.66" x2="119.38" y2="200.66" width="0.1524" layer="91"/>
<pinref part="D86" gate="G$1" pin="C"/>
<junction x="99.06" y="200.66"/>
<wire x1="99.06" y1="200.66" x2="109.22" y2="200.66" width="0.1524" layer="91"/>
<pinref part="D85" gate="G$1" pin="C"/>
<junction x="88.9" y="200.66"/>
<wire x1="88.9" y1="200.66" x2="99.06" y2="200.66" width="0.1524" layer="91"/>
<pinref part="D84" gate="G$1" pin="C"/>
<junction x="78.74" y="200.66"/>
<wire x1="78.74" y1="200.66" x2="88.9" y2="200.66" width="0.1524" layer="91"/>
<pinref part="D83" gate="G$1" pin="C"/>
<junction x="68.58" y="200.66"/>
<wire x1="68.58" y1="200.66" x2="78.74" y2="200.66" width="0.1524" layer="91"/>
<pinref part="D82" gate="G$1" pin="C"/>
<pinref part="D81" gate="G$1" pin="C"/>
<pinref part="D80" gate="G$1" pin="C"/>
<pinref part="D79" gate="G$1" pin="C"/>
<pinref part="D78" gate="G$1" pin="C"/>
<pinref part="D77" gate="G$1" pin="C"/>
<pinref part="D76" gate="G$1" pin="C"/>
<wire x1="-2.54" y1="200.66" x2="-27.94" y2="200.66" width="0.1524" layer="91"/>
<junction x="-2.54" y="200.66"/>
<wire x1="-2.54" y1="200.66" x2="7.62" y2="200.66" width="0.1524" layer="91"/>
<junction x="7.62" y="200.66"/>
<wire x1="7.62" y1="200.66" x2="17.78" y2="200.66" width="0.1524" layer="91"/>
<junction x="17.78" y="200.66"/>
<wire x1="17.78" y1="200.66" x2="27.94" y2="200.66" width="0.1524" layer="91"/>
<junction x="27.94" y="200.66"/>
<wire x1="27.94" y1="200.66" x2="38.1" y2="200.66" width="0.1524" layer="91"/>
<junction x="38.1" y="200.66"/>
<wire x1="38.1" y1="200.66" x2="48.26" y2="200.66" width="0.1524" layer="91"/>
<junction x="48.26" y="200.66"/>
<wire x1="48.26" y1="200.66" x2="58.42" y2="200.66" width="0.1524" layer="91"/>
<junction x="58.42" y="200.66"/>
<wire x1="58.42" y1="200.66" x2="68.58" y2="200.66" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="152.4" x2="-27.94" y2="200.66" width="0.1524" layer="91"/>
<junction x="-27.94" y="152.4"/>
<pinref part="USB_PWR" gate="G$1" pin="5_GND"/>
<wire x1="-27.94" y1="55.88" x2="-45.72" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="55.88" x2="-45.72" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="D26" gate="G$1" pin="A"/>
<wire x1="-2.54" y1="88.9" x2="-2.54" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R26" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="D27" gate="G$1" pin="A"/>
<wire x1="7.62" y1="78.74" x2="7.62" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R27" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="D28" gate="G$1" pin="A"/>
<wire x1="17.78" y1="78.74" x2="17.78" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R28" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="D29" gate="G$1" pin="A"/>
<wire x1="27.94" y1="78.74" x2="27.94" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R29" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="D30" gate="G$1" pin="A"/>
<wire x1="38.1" y1="78.74" x2="38.1" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R30" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="D31" gate="G$1" pin="A"/>
<wire x1="48.26" y1="78.74" x2="48.26" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R31" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="D32" gate="G$1" pin="A"/>
<wire x1="58.42" y1="78.74" x2="58.42" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R32" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="D33" gate="G$1" pin="A"/>
<wire x1="68.58" y1="78.74" x2="68.58" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R33" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="D34" gate="G$1" pin="A"/>
<wire x1="78.74" y1="78.74" x2="78.74" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R34" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="D35" gate="G$1" pin="A"/>
<wire x1="88.9" y1="78.74" x2="88.9" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R35" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="D36" gate="G$1" pin="A"/>
<wire x1="99.06" y1="88.9" x2="99.06" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R36" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="D37" gate="G$1" pin="A"/>
<wire x1="109.22" y1="78.74" x2="109.22" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R37" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="D38" gate="G$1" pin="A"/>
<wire x1="119.38" y1="88.9" x2="119.38" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R38" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="D39" gate="G$1" pin="A"/>
<wire x1="129.54" y1="78.74" x2="129.54" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R39" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="D40" gate="G$1" pin="A"/>
<wire x1="139.7" y1="88.9" x2="139.7" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R40" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="D41" gate="G$1" pin="A"/>
<wire x1="149.86" y1="78.74" x2="149.86" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R41" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="D42" gate="G$1" pin="A"/>
<wire x1="160.02" y1="88.9" x2="160.02" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R42" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="D43" gate="G$1" pin="A"/>
<wire x1="170.18" y1="78.74" x2="170.18" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R43" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="D44" gate="G$1" pin="A"/>
<wire x1="180.34" y1="88.9" x2="180.34" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R44" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="D45" gate="G$1" pin="A"/>
<wire x1="190.5" y1="78.74" x2="190.5" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R45" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="D46" gate="G$1" pin="A"/>
<wire x1="200.66" y1="88.9" x2="200.66" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R46" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="D47" gate="G$1" pin="A"/>
<wire x1="210.82" y1="78.74" x2="210.82" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R47" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="D48" gate="G$1" pin="A"/>
<wire x1="220.98" y1="88.9" x2="220.98" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R48" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="D49" gate="G$1" pin="A"/>
<wire x1="231.14" y1="78.74" x2="231.14" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R49" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="D50" gate="G$1" pin="A"/>
<wire x1="241.3" y1="88.9" x2="241.3" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R50" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="D51" gate="G$1" pin="A"/>
<wire x1="-2.54" y1="137.16" x2="-2.54" y2="127" width="0.1524" layer="91"/>
<pinref part="R51" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="D52" gate="G$1" pin="A"/>
<wire x1="7.62" y1="127" x2="7.62" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R52" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="D53" gate="G$1" pin="A"/>
<wire x1="17.78" y1="127" x2="17.78" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R53" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="D54" gate="G$1" pin="A"/>
<wire x1="27.94" y1="127" x2="27.94" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R54" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="D55" gate="G$1" pin="A"/>
<wire x1="38.1" y1="127" x2="38.1" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R55" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="D56" gate="G$1" pin="A"/>
<wire x1="48.26" y1="127" x2="48.26" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R56" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="D57" gate="G$1" pin="A"/>
<wire x1="58.42" y1="127" x2="58.42" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R57" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="D58" gate="G$1" pin="A"/>
<wire x1="68.58" y1="127" x2="68.58" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R58" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="D59" gate="G$1" pin="A"/>
<wire x1="78.74" y1="127" x2="78.74" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R59" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="D60" gate="G$1" pin="A"/>
<wire x1="88.9" y1="127" x2="88.9" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R60" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="D61" gate="G$1" pin="A"/>
<wire x1="99.06" y1="137.16" x2="99.06" y2="127" width="0.1524" layer="91"/>
<pinref part="R61" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="D62" gate="G$1" pin="A"/>
<wire x1="109.22" y1="127" x2="109.22" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R62" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="D63" gate="G$1" pin="A"/>
<wire x1="119.38" y1="137.16" x2="119.38" y2="127" width="0.1524" layer="91"/>
<pinref part="R63" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<pinref part="D64" gate="G$1" pin="A"/>
<wire x1="129.54" y1="127" x2="129.54" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R64" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<pinref part="D65" gate="G$1" pin="A"/>
<wire x1="139.7" y1="137.16" x2="139.7" y2="127" width="0.1524" layer="91"/>
<pinref part="R65" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<pinref part="D66" gate="G$1" pin="A"/>
<wire x1="149.86" y1="127" x2="149.86" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R66" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<pinref part="D67" gate="G$1" pin="A"/>
<wire x1="160.02" y1="137.16" x2="160.02" y2="127" width="0.1524" layer="91"/>
<pinref part="R67" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$68" class="0">
<segment>
<pinref part="D68" gate="G$1" pin="A"/>
<wire x1="170.18" y1="127" x2="170.18" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R68" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<pinref part="D69" gate="G$1" pin="A"/>
<wire x1="180.34" y1="137.16" x2="180.34" y2="127" width="0.1524" layer="91"/>
<pinref part="R69" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<pinref part="D70" gate="G$1" pin="A"/>
<wire x1="190.5" y1="127" x2="190.5" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R70" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<pinref part="D71" gate="G$1" pin="A"/>
<wire x1="200.66" y1="137.16" x2="200.66" y2="127" width="0.1524" layer="91"/>
<pinref part="R71" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$72" class="0">
<segment>
<pinref part="D72" gate="G$1" pin="A"/>
<wire x1="210.82" y1="127" x2="210.82" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R72" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$73" class="0">
<segment>
<pinref part="D73" gate="G$1" pin="A"/>
<wire x1="220.98" y1="137.16" x2="220.98" y2="127" width="0.1524" layer="91"/>
<pinref part="R73" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<pinref part="D74" gate="G$1" pin="A"/>
<wire x1="231.14" y1="127" x2="231.14" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R74" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$75" class="0">
<segment>
<pinref part="D75" gate="G$1" pin="A"/>
<wire x1="241.3" y1="137.16" x2="241.3" y2="127" width="0.1524" layer="91"/>
<pinref part="R75" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$76" class="0">
<segment>
<pinref part="D76" gate="G$1" pin="A"/>
<wire x1="-2.54" y1="185.42" x2="-2.54" y2="175.26" width="0.1524" layer="91"/>
<pinref part="R76" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$77" class="0">
<segment>
<pinref part="D77" gate="G$1" pin="A"/>
<wire x1="7.62" y1="175.26" x2="7.62" y2="185.42" width="0.1524" layer="91"/>
<pinref part="R77" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$78" class="0">
<segment>
<pinref part="D78" gate="G$1" pin="A"/>
<wire x1="17.78" y1="175.26" x2="17.78" y2="185.42" width="0.1524" layer="91"/>
<pinref part="R78" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<pinref part="D79" gate="G$1" pin="A"/>
<wire x1="27.94" y1="175.26" x2="27.94" y2="185.42" width="0.1524" layer="91"/>
<pinref part="R79" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$80" class="0">
<segment>
<pinref part="D80" gate="G$1" pin="A"/>
<wire x1="38.1" y1="175.26" x2="38.1" y2="185.42" width="0.1524" layer="91"/>
<pinref part="R80" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$81" class="0">
<segment>
<pinref part="D81" gate="G$1" pin="A"/>
<wire x1="48.26" y1="175.26" x2="48.26" y2="185.42" width="0.1524" layer="91"/>
<pinref part="R81" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$82" class="0">
<segment>
<pinref part="D82" gate="G$1" pin="A"/>
<wire x1="58.42" y1="175.26" x2="58.42" y2="185.42" width="0.1524" layer="91"/>
<pinref part="R82" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$83" class="0">
<segment>
<pinref part="D83" gate="G$1" pin="A"/>
<wire x1="68.58" y1="175.26" x2="68.58" y2="185.42" width="0.1524" layer="91"/>
<pinref part="R83" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$84" class="0">
<segment>
<pinref part="D84" gate="G$1" pin="A"/>
<wire x1="78.74" y1="175.26" x2="78.74" y2="185.42" width="0.1524" layer="91"/>
<pinref part="R84" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$85" class="0">
<segment>
<pinref part="D85" gate="G$1" pin="A"/>
<wire x1="88.9" y1="175.26" x2="88.9" y2="185.42" width="0.1524" layer="91"/>
<pinref part="R85" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$86" class="0">
<segment>
<pinref part="D86" gate="G$1" pin="A"/>
<wire x1="99.06" y1="185.42" x2="99.06" y2="175.26" width="0.1524" layer="91"/>
<pinref part="R86" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$87" class="0">
<segment>
<pinref part="D87" gate="G$1" pin="A"/>
<wire x1="109.22" y1="175.26" x2="109.22" y2="185.42" width="0.1524" layer="91"/>
<pinref part="R87" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$88" class="0">
<segment>
<pinref part="D88" gate="G$1" pin="A"/>
<wire x1="119.38" y1="185.42" x2="119.38" y2="175.26" width="0.1524" layer="91"/>
<pinref part="R88" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$89" class="0">
<segment>
<pinref part="D89" gate="G$1" pin="A"/>
<wire x1="129.54" y1="175.26" x2="129.54" y2="185.42" width="0.1524" layer="91"/>
<pinref part="R89" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$90" class="0">
<segment>
<pinref part="D90" gate="G$1" pin="A"/>
<wire x1="139.7" y1="185.42" x2="139.7" y2="175.26" width="0.1524" layer="91"/>
<pinref part="R90" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$91" class="0">
<segment>
<pinref part="D91" gate="G$1" pin="A"/>
<wire x1="149.86" y1="175.26" x2="149.86" y2="185.42" width="0.1524" layer="91"/>
<pinref part="R91" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$92" class="0">
<segment>
<pinref part="D92" gate="G$1" pin="A"/>
<wire x1="160.02" y1="185.42" x2="160.02" y2="175.26" width="0.1524" layer="91"/>
<pinref part="R92" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$93" class="0">
<segment>
<pinref part="D93" gate="G$1" pin="A"/>
<wire x1="170.18" y1="175.26" x2="170.18" y2="185.42" width="0.1524" layer="91"/>
<pinref part="R93" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$94" class="0">
<segment>
<pinref part="D94" gate="G$1" pin="A"/>
<wire x1="180.34" y1="185.42" x2="180.34" y2="175.26" width="0.1524" layer="91"/>
<pinref part="R94" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$95" class="0">
<segment>
<pinref part="D95" gate="G$1" pin="A"/>
<wire x1="190.5" y1="175.26" x2="190.5" y2="185.42" width="0.1524" layer="91"/>
<pinref part="R95" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$96" class="0">
<segment>
<pinref part="D96" gate="G$1" pin="A"/>
<wire x1="200.66" y1="185.42" x2="200.66" y2="175.26" width="0.1524" layer="91"/>
<pinref part="R96" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$97" class="0">
<segment>
<pinref part="D97" gate="G$1" pin="A"/>
<wire x1="210.82" y1="175.26" x2="210.82" y2="185.42" width="0.1524" layer="91"/>
<pinref part="R97" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$98" class="0">
<segment>
<pinref part="D98" gate="G$1" pin="A"/>
<wire x1="220.98" y1="185.42" x2="220.98" y2="175.26" width="0.1524" layer="91"/>
<pinref part="R98" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$99" class="0">
<segment>
<pinref part="D99" gate="G$1" pin="A"/>
<wire x1="231.14" y1="175.26" x2="231.14" y2="185.42" width="0.1524" layer="91"/>
<pinref part="R99" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$100" class="0">
<segment>
<pinref part="D100" gate="G$1" pin="A"/>
<wire x1="241.3" y1="185.42" x2="241.3" y2="175.26" width="0.1524" layer="91"/>
<pinref part="R100" gate="G$1" pin="2"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="101,1,-30.48,22.86,ONG$2,GND,,,,"/>
</errors>
</schematic>
</drawing>
</eagle>
