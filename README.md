# ColourBoard

Decorative PCB for 100 slowly changing colour LEDs. Or blinking LEDs, your choice.

Not hard to solder/assemble, so a good practice kit. However, only for the very patient, as there is a *lot* of soldering involved -- more than 400 solder points.

[Buy it from the webshop](https://autumnhippo.com/products/colourboard)

![Assembled board](https://gitlab.com/autumnhippo/colourboard/-/raw/main/media/assembledboard.jpg "Assembled board")

## Parts List

* The PCB: Order it through your favourite PCB service by sending them the newest version of the gerber files. Black soldermask is suggested, but anything goes, of course.
* 100 x colour fading LEDs, 5 mm. Usually sold on e.g. Ebay as "RGB Rainbow Slow Flashing LED". Get the ones with 2 pins only, *not* the ones with 4 pins! Alternatively, for the blinky 70'es computer effect, get LEDs typically sold as "white flashing LEDs", "flickering LEDs" or "blinking LEDs". Consider getting a few extra, as it's easy to damage one or two when soldering.
* 100 x resistors, 2 KΩ recommended, through-hole. You can of course get another value for brighter or dimmer LEDs, but 2K makes it plenty possible to run all 100 LEDs simultaneously on a standard USB port, while still being bright enough for daylight usage.
* 1 x mini-USB type B female connector for through-hole PCB-mounting. Designed for TE Connectivity 2172034-1 (Mouser 571-2172034-1). Similar connectors can be found on Ebay and other sites, often as "Mini USB Type B Female DIP 90° Angled Board PCB Socket" or similar.
* 1 x mini-USB cable
* 1 x slide switch, 2-position, SPDT On-On, 3(5) pins, 2 mm pin pitch, e.g. Mouser 611-OS102011MA1QN1

For the optional cover, you additionally need:

* The PCB cover, which is in fact also a PCB, strictly speaking, so same procedure as for the main PCB.
* 8 x machine screws (or similar), M3x3 mm or M3x4 mm (ø 3 mm, length 3 or 4 mm)
* 4 x standoffs, female-female, size M3x6 mm (ø 3 mm, length 6 mm)

![Boards](https://gitlab.com/autumnhippo/colourboard/-/raw/main/media/boards.jpg "Boards")

## Assembly instructions

1. Solder all the resistors to the board.
1. Solder the USB connector to the front and the switch to the back of the board.
1. One row at a time, solder the LEDs to the board, noting the following:
    - Make sure to not apply heat for too long on any LED, as this may relatively easily break it. A few extra LEDs are recommended and supplied with the bought kit for this reason.
    - For every row of LEDs soldered to the board, plug in the board and test it. This makes it easier to change an LED, should one turn out to not work.
    - If you will be using the front cover, I *highly* recommend to solder the LEDs with the cover already mounted, as otherwise even small inaccuracies in the positions of the LEDs will prevent the cover from being mounted afterwards.
    - Resting the board and LEDs on a soft, flat material (such as a sheet of foam) while soldering the LEDs is recommended to position the LEDs evenly and flush against the board, as this will give the finished board a nice and uniform look.
1. After testing the board and rinsing out residue flux with some isopropyl alcohol and an anti-static brush, mount the front cover using the 4 standoffs and 8 screws.

![Soldering LEDs](https://gitlab.com/autumnhippo/colourboard/-/raw/main/media/soldering.jpg "Soldering LEDs")

